# Cipta Model

php artisan make:model Namamodel

# Cipta Controller 

php artisan make:controller NamaController 

php artisan make:controller NamaController --resource 

# Cipta migration 

php artisan make:migration NamaTable


# Gabung sekali arahan di atas 

php artisan make:model NamaModel -mcr 

-m, to create migration

-c to create controller

-r to specify the controller has resource


# Tambah Kolumn baru pada table sedia ada 

php artisan make:migration add_paid_to_users_table --table=users


# Migrate

php artisan migrate 

# optimize

php artisan optimize

# serve 

php artisan serve 



