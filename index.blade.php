@extends('adminlte::page')
@section('title', 'Bilik')
@section('content_header')
    <h1>Bilik</h1> 
@stop
@section('content')

{{--@include('partials.notification')--}}
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <div class="box-header">
        @can('admin')
        <a href="/bilik/new" class="btn btn-primary float-right">Baru</a>
        @elsecan('user')
	 <a href="/bilikuser/new" class="btn btn-primary float-right">Baru</a>

	@endcan
	</div>
        <div class="box-body">
          <table id="laravel_datatable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Id</th>
		<th class="w-50">Name</th>
		<th>Alamat</th>
	        <th>Action</th>
		
              </tr>
            </thead>
	    <tbody>
		 @foreach ($biliks as $bilik)
                <tr>
                  <td>{{ $loop->iteration }}</td>
		  <td>{{ $bilik->nama }}</td>
	     	  <td>{{$bilik->alamat}}</td>
		 
		  <td>
		
			@can('admin')
			<a class="btn btn-info btn-sm" href="/bilik/{{$bilik->id}}/edit">Edit</a>
		        <a class="btn btn-danger btn-circle delete btn-sm" href="/bilik/{{ $bilik->id }}/destroy">Delete</a>
			<a class="btn btn-primary btn-sm" href="/bilik/{{ $bilik->id }}/show">Show</a>
			@elsecan('user')

			<a class="btn btn-info btn-sm" href="/bilikuser/{{$bilik->id}}/edit">Edit</a>
		        <a class="btn btn-primary btn-sm" href="/bilikuser/{{ $bilik->id }}/show">Show</a>

			@endcan
		  </td>
                </tr>
              @endforeach


	    </tbody>
          </table>
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
@stop

@section ('plugins.Toastr', true)
@section ('plugins.Datatables',true)
@section ('plugins.Sweetalert2',true)
                                                                                                   

@section('js')

@include('partials.notification')
<script>
  $(document).ready( function () {
	      $('#laravel_datatable').DataTable();
  });

 $('.delete').on('click', function (event){
	  event.preventDefault();
	  const url = $(this).attr('href'); 
	  swal.fire({
	     title: 'Are you sure?',
	     text: 'This record and it`s details will be permanantly deleted!',
	     type: 'warning',
	      showCancelButton: true,
	      confirmButtonClass: 'btn-danger',
	      confirmButtonText : 'Yes',
	     preConfirm:false 
	}).then((result) => {



		if(result.value){

		window.location.href = url;


		console.log('berjaya');
		}else if(result.dismiss === swal.DismissReason.cancel)
		{
		
		}
	  });
  });






 </script>
@stop
