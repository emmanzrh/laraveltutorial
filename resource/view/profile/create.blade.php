@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
<h1>profile</h1>
@stop

@section('content')

<!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Senarai profile</h3>

          <div class="card-tools">
		<a href="/profile/create">  
	 <button type="button" class="btn btn-tool"  data-toggle="tooltip" title="tambah">
             Tambah </button> </a>
          </div>
        </div>
        <div class="card-body">



  <form role="form" method="post">
   {{ csrf_field()}}

                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama">
                  </div>
                  <div class="form-group">
                    <label for="nama">Nric</label>
                    <input type="text" class="form-control" id="nric" name="nric" placeholder="Masukkan IC">
                  </div>

                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" rows="5" placeholder="Masukkan Alamat."></textarea>
                  </div>

                  <div class="form-group">
                    <label for="nama">Poskod</label>
                    <input type="text" class="form-control" id="poskod" name="poskod" placeholder="Masukkan Poskod">
                  </div>

                  <div class="form-group">
                    <label for="nama">DOB</label>
                    <input type="text" class="form-control" id="dob" name="dob" placeholder="Masukkan Tarikh Lahir">
                  </div>


                  <div class="form-group">
                    <label for="nama">Jawatan</label>
                     <select class="form-control" name="position" id="">
                      <option value="">Sila Pilih</option>
                      @foreach ($positions as $position)
                        <option value="{{$position->id}}" 
                          @if (old('position') == $position->id) selected @endif > {{$position->name}} </option>
                      @endforeach
                 
                     </select>
                  </div>

                  <div class="form-group">
                    <label for="nama">Asal Dari Mana</label>
                     <select class="form-control" name="state" id="">
                      <option value="">Sila Pilih</option>
                      @foreach ($states as $state)
                        <option value="{{$state->id}}" 
                          @if (old('state') == $state->id) selected @endif > {{$state->name}} </option>
                      @endforeach



                     </select>
                  </div>
                
                
                <!-- /.card-body -->

                <div class="card-footer">
		  <button type="submit" class="btn btn-primary">Submit</button>
		   <a href="{{ url()->previous() }}" class="btn btn-default">Back</a>
                </div>
              </form>


        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@stop

@section ('plugins.Toastr',true)

@section('js')
@include('partials.notification');

@stop



