@extends('adminlte::page')
@section('title', 'Users | Lara Admin')
@section('content_header')
    <h1>Users</h1>
@stop
@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <div class="box-header">
          @if(Session::has('message'))
	    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>

		  @endif   

			<a href="/users/create" class="btn btn-primary float-right">Baru</a>
  
        </div>
        <div class="box-body">
          <table id="laravel_datatable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
		
		<th>Action </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($users as $user)
                <tr>
                  <td>{{ $user->id }}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
		  @if (!empty($user->getRoleNames()))
		  @foreach ($user->getRoleNames() as $role)

			  <label class="badge badge-info">{{$role}}</label>
		  @endforeach
		  @endif	

		  </td>
		 
		  <td>


<form method="POST" action="{{ route('users.destroy',$user->id) }}">
    @csrf
    @method('delete')
			<a class="btn btn-info btn-sm" href="{{route('users.show',$user->id)}}">Show</a>
			<a class="btn btn-info btn-sm" href="{{route('users.edit',$user->id)}}">Edit</a>



    <button type="submit" class="btn btn-danger btn-sm">Delete</button> 
</form>




		 </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
@stop
@section ('plugins.Toastr', true)
@section ('plugins.Datatables',true)
@section ('plugins.Sweetalert2',true)
@section('js')
<script>
  $(document).ready( function () {
	      $('#laravel_datatable').DataTable();
	     


  });
  </script>
@stop
