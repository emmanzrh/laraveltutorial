@extends('adminlte::page')
@section('title', 'Pengguna Baru')
@section('content_header')
    <h1>Edit Pengguna </h1>
@stop
@section('content')

  <div class="row">
    <div class="col-md-12">
     
	  <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
			<form method="POST" action="{{url('users',$users->id)}}">
			@method('PATCH')
			@csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
				    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',$users->name) }}"  autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
				    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email',$users->email) }}"  autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                            </div>
                        </div>

 <div class="form-group row ">
				<label class="col-md-4 col-form-label text-md-right"> Select role</label>
				<div class="col-md-6">
				<select class="form-control " name="roles" id="roles">
				
				<option value=""> Select Role </option>
                         	 @foreach ($roles as $role)
					
					 <option value="{{$role->name}}" 

						 @if (old('roles',$role->name) == $users->getRoleNames()->contains($role->name)) selected @endif 
					 > {{$role->name}} </option>
					 	
				 @endforeach
				</select>
				</div>
                        </div>

		 
			{{$users->roles->contains($role->name)}}	
                       

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
				<button type="submit" class="btn btn-primary">
			        Edit 
				</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

   </div>
    <!-- /.col -->
  </div>
@stop

@section ('plugins.Select2', true)
@section ('plugins.Toastr',true)

@section('js')
@include('partials.notification');

<script>
  $(document).ready( function () {
	      $('#jabatan').select2();
  });


  </script>
@stop
