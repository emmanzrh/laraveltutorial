<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\Statusjabatan;
use App\Kategorijabatan;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\JabatanRequest;
class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	$jabatans = jabatan::all();
	return view('jabatan.index',compact('jabatans'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           $kategorijabatans = Kategorijabatan::orderby('id','asc')->get();
	    $statusjabatans = Statusjabatan::orderby('id','asc')->get();

	    return view('jabatan.create',compact('kategorijabatans','statusjabatans'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JabatanRequest $request)
    {
	
  	   $jabatan = new jabatan(); 
	  try {
		DB::beginTransaction();
	   
		$jabatan->nama = $request->input('nama');
		$jabatan->akronim = $request->input('akronim');
		$jabatan->email = $request->input('email');
		$jabatan->notel = $request->input('notel');

		$jabatan->kategorijabatan_id = $request->input('kategorijabatan');

		$jabatan->statusjabatan_id = $request->input('statusjabatan');

	  	    if($jabatan->save()){
		DB::commit();
		  return redirect('/jabatan')->with('successMessage', 'Maklumat jabatan Berjaya Ditambah');
	

	    }else{
		DB::rollBack();
		return back()->with('errorMessage', 'Maklumat jabatan tidak berjaya ditambah');

		}
	    } catch (\Throwable $e ){
		DB::rollBack();
	                return back()->withInput()->with('errorMessage', 'Maklumat tidak berjaya ditambah'.$e->getMessage());



    		}



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function show(Jabatan $jabatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function edit($jabatan)
    {
	    $jabatans = jabatan::findOrFail($jabatan);
	    $kategorijabatans = Kategorijabatan::orderby('id','asc')->get();
	    $statusjabatans = Statusjabatan::orderby('id','asc')->get();

	    return view('jabatan.edit',compact('statusjabatans','jabatans','kategorijabatans'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jabatan $jabatan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function destroy($jabatan)
    {
        $jabatan = Jabatan::findorFail($jabatan);
        if($jabatan->delete()){
            return redirect('/jabatan')->with('successMessage','Jabatan Telah Dipadamkan')
        }
    }
}
