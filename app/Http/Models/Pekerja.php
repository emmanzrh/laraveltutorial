<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Pekerja extends Model
{
	use HasFactory;


	use SoftDeletes;

	protected $dates = ['deleted_at','tarikhkena'];

	public function getAgeAttribute()
	{
		return Carbon::parse($this->attributes['tarikhlahir'])->age;
	}


	public function getTarikhlahirAttribute($value)
   	 {
        return Carbon::parse($value)->format('d/m/Y');
	}

	public function getNamaAttribute($value){
		return strtoupper($value);
	}



	
	public function negeri(){

		return $this->hasOne('App\Negeri','id','negeri_id');

	}



	public function state(){

		return $this->hasOne('App\Models\State','id','state_id');

	}

    public function position(){

		return $this->hasOne('App\Models\Position','id','position_id');

	}

    public function profile(){

		return $this->belongsTo('App\Models\Profile');

	}


}
