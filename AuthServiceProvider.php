<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
	  //  'App\Tempahan' => 'App\Policies\TempahanPolicy',
	    Tempahan::class => TempahanPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

			
 	 Gate::define('admin', function ($user) {
  	         //  if ($user->role == 'super'){
  	           	               return $user->role== 'super';
		//		                  }
		   //           return false;
		          });

	 Gate::define('user', function ($user) {
  	           //if ($user->role == 'user'){
  	           	               return $user->role=='user';
		//		                  }
		     //         return false;
		          });
  	      
	 Gate::define('luar', function ($user) {
  	           //if ($user->role == 'luar'){
  	           	               return $user->role=='luar';
		//		                  }
		     //         return false;
		          });
  	      
	


    }
}
