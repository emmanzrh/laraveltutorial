## DAY 1

| Jam   | Agenda |
| ------ | ------ |
| 8:30 - 9:30 | Setup Laragon, VSCode , git |
| 9:30-10:00  | Pengenalan Kepada Laravel |
| 10:00-10:15  | Take 15 |
| 11:15-12:45  | Database Design |
| 1:00-2:00  |  Rehat |
| 2:00-4:00  |  Laravel MVC- User login , Laravel MVC - Setup AdminLTE| 

## DAY 2
| Jam   | Agenda |
| ------ | ------ |
| 8:30-10:30 |  Laravel - Create Route Model | 
| 10:30-10:45 | Take 15 | 
| 10:45-1:00| Create Controller | 
| 1:00-2:00  |  Rehat |
| 2:00-4:00  | One to one, One to Many Foreign Key| 

## DAY 3

| Jam | Agenda |
|------| -----|
|8:30-10:30| CRUD |
|10:30-10:45| Datatable |
|10:45-11:00 | Take 15 |
|11:00-1:00|       |
